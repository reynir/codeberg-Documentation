---
eleventyNavigation:
  key: Integrations
  title: Integrations with Other Services
  icon: puzzle-piece
  order: 70
---

These documentation pages contain information on how you can use third-party software with Codeberg.

See also [awesome-gitea](https://gitea.com/gitea/awesome-gitea/src/branch/master/README.md) which lists projects that work with Gitea, the software which Codeberg is based on.
