---
eleventyNavigation:
  key: LicensingOnCodeberg
  title: Licensing on Codeberg
  parent: GettingStarted
  order: 70
---

Codeberg's mission is to support the development and creation of Free Software, thus we only allow those repos licensed under an OSI/FSF-approved license.

Software is considered [free software](https://www.gnu.org/philosophy/free-sw.html) if it's users have the following four freedoms (according to the [GNU free software philosophy](https://www.gnu.org/philosophy/free-sw.html#four-freedoms):

> * The freedom to run the program as you wish, for any purpose (freedom 0).
> * The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1). Access to the source code is a precondition for this.
> * The freedom to redistribute copies so you can help others (freedom 2).
> * The freedom to distribute copies of your modified versions to others (freedom 3). By doing this you can give the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.

This page highlights some of the major topics to think about when licensing your code and aims to make it easy to choose one quickly. However, do make sure to dedicate more time to this if you have very specific goals in mind or expect a significant impact of your code.

## Copyleft

One of the main principles that's important to understand is [*copyleft*](https://www.gnu.org/philosophy/categories.html#CopyleftedSoftware). Very simply stated, copyleft is a way to make sure free software stays free. It is the rule that when redistributing the program, you cannot add restrictions to deny others the freedoms stated above. It is important to think about this concept as you choose a license to determine how you want your work to be used, because as stated by [the GNU guide](https://www.gnu.org/philosophy/categories.html#Non-CopyleftedFreeSoftware):

> If a program is free but not copylefted, then some copies or modified versions may not be free at all. A software company can compile the program, with or without modifications, and distribute the executable file as a proprietary software product.

An example of one of the strongest copyleft licenses is the [GNU Affero General Public License](https://www.gnu.org/licenses/why-affero-gpl.html). It adds a requirement that even when a modified program is run on a server and lets other users communicate with it, your server must also allow them to download the source code corresponding to the modified version.

The other end of the spectrum is licensing your software under no restrictions at all. An example of this is the [0-Clause BSD license](https://spdx.org/licenses/0BSD.html). A completely unrestricted license means that the software is *free* (per the definition above) but any copies/modifications of it may not be. *Not* specifying a license for your code does not automatically mean you've made it available without restrictions; you are still its copyright holder and must explicitly make the code free by choosing a free license.

## Recommended Licenses

The [list of OSI-approved licenses](https://opensource.org/licenses) highlights a couple of popular licenses that are available for use on Codeberg, and similar to [Choosealicense.com](https://choosealicense.com/licenses/) this is a shortlist of Codeberg's recommendations sorted from highly protective to unconditional:

* [GNU Affero General Public License](https://www.gnu.org/licenses/agpl-3.0.html)
* [GNU General Public License (GPL)](https://www.gnu.org/licenses/gpl-3.0.html)
* [MIT license](https://opensource.org/licenses/MIT)
* [0-Clause BSD license](https://spdx.org/licenses/0BSD.html)

For more licenses, more details and some considerations when using any of them, see the [GNU list of licenses](https://www.gnu.org/licenses/license-list.html). Please do not use custom licenses, [they are usually a bad idea](https://en.wikipedia.org/wiki/License_proliferation) and might result in legal uncertainty. 

## Non-code Licensing

For documentation, writing and other non-code assets a [Creative Commons (CC)](https://creativecommons.org/about/cclicenses/) license can be used, but note that only the following CC licenses are considered free (again sorted from protective to unconditional):

* [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/)
* [CC-BY](https://creativecommons.org/licenses/by/4.0/)
* [CC0](https://creativecommons.org/publicdomain/zero/1.0/)

Like CC themselves, Codeberg **recommends against** using a [CC license on code](https://creativecommons.org/faq/#can-i-apply-a-creative-commons-license-to-software).
