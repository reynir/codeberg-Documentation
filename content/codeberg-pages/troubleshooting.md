---
eleventyNavigation:
  key: CodebergPagesTroubleshooting
  title: Troubleshooting
  parent: CodebergPages
  order: 99
---

## My web browser displays a security warning when I try to access my Codeberg Pages.
If your user name contains a dot (e.g. `user.name`) your Codeberg Pages URL (https://user.name.codeberg.page) contains a sub-sub-domain which does not work with Let's Encrypt wildcard certificates. Use the alternative URL https://pages.codeberg.org/user.name/ as a workaround.
